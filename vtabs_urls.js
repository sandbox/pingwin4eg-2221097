(function ($) {

if (Drupal.verticalTab != undefined) {
  var focus = Drupal.verticalTab.prototype.focus;
  Drupal.verticalTab.prototype = $.extend(Drupal.verticalTab.prototype,
    {
      /**
       * Changes URL fragment part.
       */
      focus: function () {
        $.proxy(focus, this)();

        $('.vertical-tabs-panes').once('vtabs-urls', function () {
          var $fieldsets = $('> fieldset', this);
          $fieldsets.each(function () {
            var vertical_tab = $(this).data('verticalTab');
            var id = $(vertical_tab.fieldset).find('.vtabs-urls-id').attr('id');

            vertical_tab.link.attr('href', '#' + id);
            vertical_tab.link.unbind('click').click(function () {
              vertical_tab.focus();
            });
          });
        });
      }
    }
  );
}

})(jQuery);
